<?php

namespace Drupal\language_negotiation_specific_url\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\language_negotiation_specific_url\Plugin\LanguageNegotiation\LanguageNegotiationSpecificUrl;

/**
 * Configure the URL language negotiation method for this site.
 *
 * @internal
 */
class NegotiationUrlSpecificForm extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new NegotiationUrlSpecificForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager) {
    parent::__construct($config_factory);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'language_negotiation_configure_specific_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['language.negotiation.specific.url'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    $form['#tree'] = TRUE;
    $config = $this->config('language.negotiation.specific.url');
    $languages = $this->languageManager->getLanguages();
    $options = [];
    foreach ($languages as $langcode => $language) {
      $t_args = ['%language' => $language->getName(), '%langcode' => $language->getId()];
      $form['domain_lang'][$langcode] = [
        '#type' => 'details',
        '#title' =>  $this->t('%language specific Path/Domain Url', ['%language' => $language->getName()]),
        '#open' => FALSE,
        '#description' => $this->t('Language codes or other custom text to use as a path prefix for URL language detection. For the selected fallback language, this value may be left blank. <strong>Modifying this value may break existing URLs. Use with caution in a production environment.</strong> Example: Specifying "deutsch" as the path prefix code for German results in URLs like "example.com/deutsch/contact".'),
      ];
      $form['domain_lang'][$langcode]['language_negotiation_url_part'] = [
        '#title' => $this->t('Path/Domain prefix'),
        '#type' => 'radios',
        '#options' => [
          LanguageNegotiationSpecificUrl::CONFIG_PATH_PREFIX => $this->t('Path prefix'),
          LanguageNegotiationSpecificUrl::CONFIG_DOMAIN => $this->t('Domain'),
        ],
        '#default_value' => !empty($config->get('domain_lang.' . $langcode . '.language_negotiation_url_part')) ?
        $config->get('domain_lang.' . $langcode . '.language_negotiation_url_part') : NULL,
      ];
      $form['domain_lang'][$langcode]['prefix'] = [
        '#type' => 'textfield',
        '#title' => $language->isDefault() ? $this->t('%language (%langcode) path prefix (Default language)', $t_args) : $this->t('%language (%langcode) path prefix', $t_args),
        '#maxlength' => 64,
        '#default_value' => !empty($config->get('domain_lang.' . $langcode . '.prefixes')) ?
        $config->get('domain_lang.' . $langcode . '.prefixes') : NULL,
        '#field_prefix' => $base_url . '/',
        '#states' => [
          'visible' => [
            ':input[name="domain_lang['.$langcode.'][language_negotiation_url_part]"]' => [
              'value' => (string) LanguageNegotiationSpecificUrl::CONFIG_PATH_PREFIX,
            ],
          ],
        ],
      ];
      $form['domain_lang'][$langcode]['domain'] = [
        '#type' => 'textfield',
        '#title' => $this->t('%language (%langcode) domain', ['%language' => $language->getName(), '%langcode' => $language->getId()]),
        '#maxlength' => 128,
        '#default_value' => !empty($config->get('domain_lang.' . $langcode . '.domains')) ?
        $config->get('domain_lang.' . $langcode . '.domains') : NULL,
        '#states' => [
          'visible' => [
            ':input[name="domain_lang['.$langcode.'][language_negotiation_url_part]"]' => [
              'value' => (string) LanguageNegotiationSpecificUrl::CONFIG_DOMAIN,
            ],
          ],
        ],
      ];
      $options[$langcode] = $language->getName();
    }
    $form_state->set('lang_options', $options);
    $form_state->setRedirect('language.negotiation');

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $languages = $this->languageManager->getLanguages();
    $default_langcode = $this->config('language.negotiation')->get('selected_langcode');
    if ($default_langcode == LanguageInterface::LANGCODE_SITE_DEFAULT) {
      $default_langcode = $this->languageManager->getDefaultLanguage()->getId();
    }

    foreach ($languages as $langcode => $language) {
      $value = $form_state->getValue(['domain_lang', $langcode, 'language_negotiation_url_part']);
      $prefix = $form_state->getValue(['domain_lang', $langcode, 'prefix']);
      $domain = $form_state->getValue(['domain_lang', $langcode, 'domain']);
      $count[] =  trim($form_state->getValue(['domain_lang', $langcode, 'prefix']));
      $domainCount[] =  trim($form_state->getValue(['domain_lang', $langcode, 'domain']));
      if ($value == '') {
        $form_state->setErrorByName("['domain_lang', $langcode, 'language_negotiation_url_part']", $this->t('Please select path or domain prefix'));
      }
      else if ($value == LanguageNegotiationSpecificUrl::CONFIG_PATH_PREFIX
       && $prefix == '' &&  $langcode != $default_langcode) {
          $form_state->setErrorByName("['domain_lang', $langcode, 'prefix']", $this->t('Please enter unique lang code for %language language <a href=":url">selected detection fallback language.</a>',
          ['%language' => $language->getName() ,':url' => Url::fromRoute('entity.configurable_language.collection')->toString()]));
      }
      elseif ($value == LanguageNegotiationSpecificUrl::CONFIG_PATH_PREFIX && strpos($prefix, '/') !== FALSE) {
        // Throw a form error if the string contains a slash,
        // which would not work.
        $form_state->setErrorByName("['domain_lang', $langcode, 'prefix']", $this->t('The prefix may not contain a slash.'));
      }
      foreach(array_count_values(array_filter($count)) as $langPrefix) {
        if ($langPrefix > 1 && $value == LanguageNegotiationSpecificUrl::CONFIG_PATH_PREFIX) {
          $form_state->setErrorByName("['domain_lang', $langcode, 'prefix']", $this->t('The domain for %language, %prefix, is not unique.', ['%language' => $language->getName(), '%prefix' => $prefix]));
        }
      }

      if ($value == LanguageNegotiationSpecificUrl::CONFIG_DOMAIN
       && $domain == '') {
          $form_state->setErrorByName("['domain_lang', $langcode, 'domain']", $this->t('The domain for %language, should not empty.', ['%language' => $language->getName()]));
      }

      else if ($value == LanguageNegotiationSpecificUrl::CONFIG_DOMAIN
      && !empty($domain)) {
        $host = 'http://' . str_replace(['http://', 'https://'], '', $domain);
        if (parse_url($host, PHP_URL_HOST) != $domain) {
          $form_state->setErrorByName("['domain_lang', $langcode, 'domain']", $this->t('The domain for %language may only contain the domain name, not a trailing slash, protocol and/or port.', ['%language' => $language->getName()]));
        }
      }

      foreach(array_count_values(array_filter($domainCount)) as $domainPrefix) {
        if ($domainPrefix > 1 && $value == LanguageNegotiationSpecificUrl::CONFIG_DOMAIN) {
          $form_state->setErrorByName("['domain_lang', $langcode, 'domain']", $this->t('The domain for %language, %domain, is not unique.', ['%language' => $language->getName(), '%domain' => $domain]));
        }
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save selected format (prefix or domain).
    $config = $this->config('language.negotiation.specific.url');
    $langConfig = $form_state->get('lang_options');
    foreach ($langConfig as $lang_code => $data) {
      $config->set('domain_lang.' . $lang_code . '.language_negotiation_url_part',
      $form_state->getValue(['domain_lang', $lang_code, 'language_negotiation_url_part']));
      $config->set('domain_lang.' . $lang_code . '.prefixes',
      $form_state->getValue(['domain_lang', $lang_code, 'prefix']));
      $config->set('domain_lang.' . $lang_code . '.domains',
      $form_state->getValue(['domain_lang', $lang_code, 'domain']));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
